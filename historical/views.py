from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.db import connection
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from django.shortcuts import redirect, render
from .forms import ResourceForm, QueryForm
from .models import Resource, Metadata, Theme

from django.contrib import messages
# Imports nécessaires pour le signUp
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

from django.views.generic import View

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from .function import *
from dotenv import load_dotenv
import os

# pour chaîner des QuerySet
from itertools import chain

import asyncio

from asgiref.sync import async_to_sync, sync_to_async

intent = ""

load_dotenv()
api_key_youtube = os.getenv("api_key_youtube")
api_key_luis = os.getenv("api_key_luis")
api_key_auth_luis = os.getenv("api_key_auth_luis")

@login_required
def accueil(request):
    return render(request, 'historical/home.html')

@login_required
def storage(request):
    model = Resource()
    # On récupère la liste des thèmes existants
    # themes = Theme.objects.values_list('theme', flat=True)
    user_id = request.user.id
    with connection.cursor() as cursor:
        cursor.execute("""SELECT theme FROM historical_theme 
                       JOIN auth_user ON auth_user.id = historical_theme.user_id_id 
                       WHERE historical_theme.user_id_id=%s""", [user_id])
        themes_tuple = cursor.fetchall()
        
    themes = []
    if themes_tuple is not None:
        print(themes_tuple)
        for theme in themes_tuple:
            themes.append(theme[0])
        
    
    if request.method == 'POST':
        form = ResourceForm(request.POST)
        if form.is_valid():
            selected_theme = request.POST.get('themes')
            res = form.save(commit=False)
            validate = URLValidator()
            # On test si l'url est valide
            try:
                validate(res.resource_link)
            
                # if res is already in Resource table, display error message below form
                if Resource.objects.filter(resource_link=res.resource_link, rec_author=request.user).exists():
                    
                    # display error message below the form
                    # https://docs.djangoproject.com/en/3.1/ref/forms/api/#django.forms.Form.add_error

                    messages.error(request, 'Cette ressource figure déjà dans votre historique')
                    messages.error(request, form.errors)
                else:
                    res.rec_author = request.user
                    with connection.cursor() as cursor:
                        cursor.execute("SELECT id FROM historical_theme WHERE theme LIKE %s", [selected_theme])
                        selected_theme_id = cursor.fetchone()[0]
                    selected_theme = Theme(id=selected_theme_id)
                    res.rec_theme = selected_theme
                    domain_url = get_domain_url(res.resource_link)
                    if 'youtube' in domain_url:
                        youtube = extract_youtube(api_key_youtube, res.resource_link)
                        res.save()
                        met = Metadata(title=youtube[1], res_author=youtube[2], pub_date=youtube[0], source=domain_url,link_ref=res) 
                    else:
                        metadata = get_metadata(res.resource_link)
                        title = metadata['title']
                        description = metadata['description']
                        res.save()
                        met = Metadata(title=title, description=description, source=domain_url, link_ref=res) 
                    
                    met.save()
                    
                    # https://docs.djangoproject.com/en/3.1/ref/contrib/messages/#django.contrib.messages.success
                    messages.success(request, 'Votre nouvelle ressource a bien été enregistrée')
                        
                
                    return render(request, 'historical/storage.html', {'form': form, 'themes': themes})
            # Si l'url n'est pas valide, on notifie l'utilisateur avec un message
            except ValidationError:
                messages.error(request, 'Veuillez entrer un url valide')
                messages.error(request, form.errors)
               
    else:
        form = ResourceForm()
    
    return render(request, 'historical/storage.html', {'form': form, 'themes': themes})

@login_required
def access(request):
    return render(request, 'historical/access.html')


# fonction pour la déconnection qui génère le template logout.html
@login_required
def disconnect(request):
    logout(request)
    return render(request, 'historical/logout.html')
    #return render(request,'blog/logout.html'

# Permettre l'ajout d'un nouveau thème
@login_required
def add_theme(request):
    if request.method == 'POST':
        user_id = request.user.id
        new_theme = request.POST.get('theme')
        # available_themes = Theme.objects.values_list('theme', flat=True)
        with connection.cursor() as cursor:
            cursor.execute("""SELECT historical_theme.theme
                            FROM historical_theme
                            WHERE historical_theme.user_id_id = %s""", [user_id])
            available_themes = cursor.fetchall()
        if new_theme not in available_themes:
            Theme.objects.create(theme=new_theme.lower(), user_id=request.user)
            add_theme_luis(new_theme, api_key_auth_luis)
            train_luis(api_key_auth_luis)
            publish_luis(api_key_auth_luis)
            messages.success(request, "Le thème a bien été ajouté")
        else:
            messages.error(request, 'Ce thème figure déjà dans la base de données')
        
        # return render(request, "historical/theme.html")
        response = redirect("../../historical/enregistrer")
        return response
        
    else:
        return render(request, 'historical/theme.html')

# Classe liée au form signup permettant de créer un nouvel utilisateur  
class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"

@login_required
@csrf_exempt
def ajax(request):

    if request.POST.get("intent") in "chercher une ressource":
        user_id = request.user.id     
         
        with connection.cursor() as cursor: 
            print("################") 
            print(request.POST.get("intent"),"intent", type(request.POST.get("intent")))
            print(request.POST.get("theme"),"theme", type(request.POST.get("theme")))
            print(request.POST.get("format"),"format",type(request.POST.get("format")))
            print(request.POST.getlist("keywords[]"),"keywords",type(request.POST.getlist("keywords[]")))
            print("################") 
            
            # buiding full query
            theme = request.POST.get("theme")
            format = request.POST.get("format") 
            keywords = request.POST.getlist("keywords[]")
                
            
            if theme is None and format is None:
                cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme 
                FROM historical_metadata
                JOIN historical_resource
                ON historical_metadata.link_ref_id = historical_resource.id
                JOIN historical_theme
                ON historical_resource.rec_theme_id = historical_theme.id
                JOIN auth_user
                ON auth_user.id = historical_resource.rec_author_id
                WHERE auth_user.id = %s""", [user_id]) 
                
            elif format is None:
                cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme 
                FROM historical_metadata
                JOIN historical_resource
                ON historical_metadata.link_ref_id = historical_resource.id
                JOIN historical_theme
                ON historical_resource.rec_theme_id = historical_theme.id
                JOIN auth_user
                ON auth_user.id = historical_resource.rec_author_id
                WHERE auth_user.id = %s AND historical_theme.theme LIKE %s""", [user_id, theme])  

            elif theme is None:
                cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme 
                FROM historical_metadata
                JOIN historical_resource
                ON historical_metadata.link_ref_id = historical_resource.id
                JOIN historical_theme
                ON historical_resource.rec_theme_id = historical_theme.id
                JOIN auth_user
                ON auth_user.id = historical_resource.rec_author_id
                WHERE auth_user.id = %s AND historical_resource.rec_format LIKE %s""", [user_id, format.capitalize()])
                
            else:
                cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme
                FROM historical_metadata
                JOIN historical_resource
                ON historical_metadata.link_ref_id = historical_resource.id
                JOIN historical_theme
                ON historical_resource.rec_theme_id = historical_theme.id
                JOIN auth_user
                ON auth_user.id = historical_resource.rec_author_id
                WHERE auth_user.id = %s AND historical_theme.theme LIKE %s and historical_resource.rec_format LIKE %s""", [user_id, theme, format.capitalize()])
                
            rows = cursor.fetchall()
            print(rows)       
            
            if keywords != "" and keywords != []:
                # Transformer la liste de keywords en string
                print(keywords)
                str_keywords = ""
                for keyword in keywords:
                    str_keywords += " " + keyword
                    
                # Récupérer le contenu et le lien de chaque ressource
                contents = []
                links = []
                for resource in rows:
                    link = resource[2]
                    links.append(link)
                    domain_url = get_domain_url(link)
                    if "youtube" in domain_url:
                        contents.append(extract_youtube(api_key_youtube, link)[3])
                    else:
                        contents.append(get_content(link)["content"])
                
                # Obtenir les cos similarities entre les keywords et le contenu textuel des ressources
                similarities = get_cos_similarity(contents, str_keywords)
                print(similarities)
                print(links)
                
                # Pour chaque ressource, updater la valeur de la similarité
                for link, similarity in zip(links, similarities):
                    cursor.execute("""UPDATE historical_resource
                                SET similarity=%s
                                WHERE resource_link LIKE %s AND rec_author_id=%s""", [round(similarity, 3), link, user_id])
                        
                if theme is None and format is None:
            
                    # Afficher les ressources par ordre décroissant de similarité
                    cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme, historical_resource.similarity 
                        FROM historical_metadata
                        JOIN historical_resource
                        ON historical_metadata.link_ref_id = historical_resource.id
                        JOIN historical_theme
                        ON historical_resource.rec_theme_id = historical_theme.id
                        JOIN auth_user
                        ON auth_user.id = historical_resource.rec_author_id
                        WHERE auth_user.id = %s 
                        ORDER BY historical_resource.similarity DESC""", [user_id])
                    
                    rows = cursor.fetchall()
                elif format is None:
                    # Afficher les ressources par ordre décroissant de similarité
                    cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme, historical_resource.similarity 
                        FROM historical_metadata
                        JOIN historical_resource
                        ON historical_metadata.link_ref_id = historical_resource.id
                        JOIN historical_theme
                        ON historical_resource.rec_theme_id = historical_theme.id
                        JOIN auth_user
                        ON auth_user.id = historical_resource.rec_author_id
                        WHERE auth_user.id = %s AND historical_theme.theme LIKE %s
                        ORDER BY historical_resource.similarity DESC""", [user_id, theme])
                    
                    rows = cursor.fetchall()
                    
                elif theme is None:
                    # Afficher les ressources par ordre décroissant de similarité
                    cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme, historical_resource.similarity 
                        FROM historical_metadata
                        JOIN historical_resource
                        ON historical_metadata.link_ref_id = historical_resource.id
                        JOIN historical_theme
                        ON historical_resource.rec_theme_id = historical_theme.id
                        JOIN auth_user
                        ON auth_user.id = historical_resource.rec_author_id
                        WHERE auth_user.id = %s AND historical_resource.rec_format LIKE %s
                        ORDER BY historical_resource.similarity DESC""", [user_id, format.capitalize()])
                    
                    rows = cursor.fetchall()
                    
                else:
                    # Afficher les ressources par ordre décroissant de similarité
                    cursor.execute("""SELECT historical_resource.rec_date,historical_metadata.title, historical_resource.resource_link, historical_metadata.source, historical_resource.rec_format, historical_theme.theme, historical_resource.similarity 
                        FROM historical_metadata
                        JOIN historical_resource
                        ON historical_metadata.link_ref_id = historical_resource.id
                        JOIN historical_theme
                        ON historical_resource.rec_theme_id = historical_theme.id
                        JOIN auth_user
                        ON auth_user.id = historical_resource.rec_author_id
                        WHERE auth_user.id = %s AND historical_resource.rec_format LIKE %s
                        AND historical_theme.theme LIKE %s
                        ORDER BY historical_resource.similarity DESC""", [user_id, format.capitalize(), theme])
                    
                    rows = cursor.fetchall()
            
                   
            
            
    return JsonResponse({
        "data": rows
    })




