from pydoc import describe
from django.db import models
from django.contrib.auth.models import User


# Ici, chaque modèle est représenté par une classe qui hérite de django.db.models.Model. 
# Chaque modèle possède des variables de classe, 
# chacune d’entre elles représentant un champ de la base de données pour ce modèle.

class Theme(models.Model):
    theme = models.CharField(max_length=50, unique=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    
class Resource(models.Model):
    
    # j'ai mis textfield au lieu de Charfield pour ne pas avoir la limitation des caractères
    resource_link = models.CharField(max_length=1000, unique=False, null=False)
    rec_date = models.DateTimeField(auto_now=True)
    
    # il ne fait pas le lien avec le user constraint not null
    rec_author = models.ForeignKey(User, on_delete=models.CASCADE)
    rec_theme = models.ForeignKey(Theme, unique=False, null=False, on_delete=models.CASCADE)
    rec_format = models.TextField(unique=False, choices= [('Video','Video'),('Texte','Texte'),('Audio','Audio')], null=False)
    rec_comment = models.TextField(unique=False, null=True)
    similarity = models.FloatField(unique=False, null=True)    

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['resource_link', 'rec_author'], name='couple_ressource_utiisateur_unique')
        ]

class Metadata(models.Model):
    title = models.TextField(unique=False, null=False)
    link_ref = models.ForeignKey(Resource, on_delete=models.CASCADE)
    description = models.TextField(unique=False, null=True)
    res_author = models.TextField(unique=False, null=True)
    pub_date = models.DateTimeField(null=True)
    source = models.TextField(unique=False, null=True)
    
class Query(models.Model):
    user_query = models.TextField(unique=False, null=False)
    search_date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    