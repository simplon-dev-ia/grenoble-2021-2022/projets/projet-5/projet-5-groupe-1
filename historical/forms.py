from django.forms import ModelForm
from .models import Resource, Query


class ResourceForm(ModelForm):
    class Meta:
        model = Resource
        # https://docs.djangoproject.com/en/3.1/topics/forms/modelforms/
        fields = ['resource_link', 'rec_format', 'rec_comment']
        

    def __init__(self, *args, **kwargs):
        super(ResourceForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class QueryForm(ModelForm):
    class Meta:
        model = Query
        # https://docs.djangoproject.com/en/3.1/topics/forms/modelforms/
        fields = ['user_query']

    def __init__(self, *args, **kwargs):
        super(QueryForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})
