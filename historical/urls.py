from django.urls import path
from django.contrib.auth.views import LogoutView
from . import views

# Import pour le signup
from .views import SignUpView

urlpatterns = [
    # le name correspond à la dénomination html dans le fichier base.html
    path('', views.accueil, name='accueil'),
    path('enregistrer/', views.storage, name='storage'),
    path('consulter/', views.access, name='access'),
    path("deconnection/", views.disconnect, name="deconnect"),
    path("signup/", SignUpView.as_view(), name="signup"),
    path("theme/", views.add_theme, name="theme"),
    path('demo/', views.ajax, name="ajax")
    
    #path('deconnection/', views.deconnect, name = 'deconnect')
    
    
    #path("logout", views.logout_request, name= "logout"),
    #path('accéder/', views.reclamations, name='access'),

]