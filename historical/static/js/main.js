var messages = [], //array that hold the record of each string in chat
  lastUserMessage = "", //keeps track of the most recent input string from the user
  botMessage = "", //var keeps track of what the chatbot is going to say
  botName = 'Chatbot' //name of the chatbot
let intent = "",
  format = "",
  theme = "",
  keywords = ""
//edit this function to change what the chatbot says
function chatbotResponse() {
  params = encodeURI(lastUserMessage)
  url = "https://luisaragrenoble.cognitiveservices.azure.com/luis/prediction/v3.0/apps/296432c0-75a8-452e-b9f2-900416c422ab/slots/staging/predict?verbose=true&show-all-intents=true&log=true&subscription-key=bba66aa8e81347349a2bed28a28981d2&query=" + params

  $.ajax(url, {
    type: 'GET',  // http method
    success: function (data) {
      intent = data.prediction.topIntent;
      if (typeof data.prediction.entities.keyword === "undefined") {
        keywords = ""
        console.log("No keyword")
      }
      else {
        keywords = data.prediction.entities.keyword
        console.log(keywords)
      }
      if (typeof data.prediction.entities.format === "undefined" && typeof data.prediction.entities.theme === "undefined") {
        console.log()
        $.ajax('/historical/demo/', {
          type: 'POST',  // http method
          data: { "intent": intent, "keywords": keywords },  // data to submit
          success: function (response) {
            console.log(response.data)

            $('#tbody').html("");
            for (i = 0; i < response.data.length; i++) {
              let ligne =
                '<tr> <td>' + response.data[i][0] + '</td>' +
                '<td>' + response.data[i][1].link(response.data[i][2]) + '</td>' +
                '<td>' + response.data[i][3] + '</td>' +
                '<td>' + response.data[i][4] + '</td>' +
                '<td>' + response.data[i][5] + '</td>' +
                '<td>' + response.data[i][6] + '</td></tr>'
              $('#tbody').append(ligne);
            }
          }
        });
      }

      else if (typeof data.prediction.entities.theme === "undefined") {
        format = data.prediction.entities.format[0][0];
        $.ajax('/historical/demo/', {
          type: 'POST',  // http method
          data: { "intent": intent, "format": format, "keywords": keywords },  // data to submit
          success: function (response) {
            console.log(response.data)
            $('#tbody').html("");
            for (i = 0; i < response.data.length; i++) {
              let ligne =
                '<tr> <td>' + response.data[i][0] + '</td>' +
                '<td>' + response.data[i][1].link(response.data[i][2]) + '</td>' +
                '<td>' + response.data[i][3] + '</td>' +
                '<td>' + response.data[i][4] + '</td>' +
                '<td>' + response.data[i][5] + '</td>' +
                '<td>' + response.data[i][6] + '</td></tr>'
              $('#tbody').append(ligne);
            }
          }
        });
      }
      else if (typeof data.prediction.entities.format === "undefined") {
        theme = data.prediction.entities.theme[0][0];
        $.ajax('/historical/demo/', {
          type: 'POST',  // http method
          data: { "intent": intent, "theme": theme, "keywords": keywords },  // data to submit
          success: function (response) {
            console.log(response.data)
            $('#tbody').html("");
            for (i = 0; i < response.data.length; i++) {
              let ligne =
                '<tr> <td>' + response.data[i][0] + '</td>' +
                '<td>' + response.data[i][1].link(response.data[i][2]) + '</td>' +
                '<td>' + response.data[i][3] + '</td>' +
                '<td>' + response.data[i][4] + '</td>' +
                '<td>' + response.data[i][5] + '</td>' +
                '<td>' + response.data[i][6] + '</td> </tr>'
              $('#tbody').append(ligne);
            }
          }
        });
      }

      else {
        theme = data.prediction.entities.theme[0][0];
        format = data.prediction.entities.format[0][0];
        $.ajax('/historical/demo/', {
          type: 'POST',  // http method
          data: { "intent": intent, "format": format, "theme": theme, "keywords": keywords },  // data to submit
          success: function (response) {
            $('#tbody').html("");
            for (i = 0; i < response.data.length; i++) {
              console.log(response.data[i][1].link(response.data[i][2]))
              let ligne =
                '<tr> <td>' + response.data[i][0] + '</td>' +
                '<td>' + response.data[i][1].link(response.data[i][2]) + '</td>' +
                '<td>' + response.data[i][3] + '</td>' +
                '<td>' + response.data[i][4] + '</td>' +
                '<td>' + response.data[i][5] + '</td>' +
                '<td>' + response.data[i][6] + '</td> </tr>'
              $('#tbody').append(ligne);
            }
          }
        });
      }
    },
    error: function (jqXhr, textStatus, errorMessage) {
      console.log("error")
    }
  });

  botMessage = "Voici votre requete"; //the default message

  if (intent === 'chercher une ressource') {
    botMessage = 'Voici vos ressources!';
  }

  if (lastUserMessage === 'ok') {
    botMessage = 'Here are the ressources that you ask!';
  }

  if (lastUserMessage === 'name') {
    botMessage = 'My name is ' + botName;
  }
}

//
//this runs each time enter is pressed.
//It controls the overall input and output
function newEntry() {
  //if the message from the user isn't empty then run 
  if (document.getElementById("chatbox").value != "") {
    //pulls the value from the chatbox ands sets it to lastUserMessage
    lastUserMessage = document.getElementById("chatbox").value;
    //sets the chat box to be clear
    document.getElementById("chatbox").value = "";
    //adds the value of the chatbox to the message array
    messages.push(lastUserMessage);
    //takes the return value from chatbotResponse() and outputs it
    chatbotResponse()
    //add the chatbot's name and message to the array messages
    messages.push("<b>" + botName + ":</b> " + botMessage)
    // says the message using the text to speech function written below
    Speech(botMessage);
    //outputs the last few messages to html
    for (var i = 1; i < 8; i++) {
      if (messages[messages.length - i])
        document.getElementById("chatlog" + i).innerHTML = messages[messages.length - i];
    }
  }
}

//text to Speech
//https://developers.google.com/web/updates/2014/01/Web-apps-that-talk-Introduction-to-the-Speech-Synthesis-API
function Speech(say) {
  if ('speechSynthesis' in window) {
    var utterance = new SpeechSynthesisUtterance(say);
    //utterance.volume = 1; // 0 to 1
    //utterance.rate = 0.5; // 0.1 to 10
    //utterance.pitch = 2; //0 to 2
    //utterance.text = 'Hello World';
    //utterance.lang = 'en-US';
    speechSynthesis.speak(utterance);
  }
}

//runs the keypress() function when a key is pressed
document.onkeypress = keyPress;
//if the key pressed is 'enter' runs the function newEntry()
function keyPress(e) {
  var x = e || window.event;
  var key = (x.keyCode || x.which);
  if (key == 13 || key == 3) {
    //runs this function when enter is pressed
    newEntry();
  }
}