import re
from googleapiclient.discovery import build
import requests
import urllib.parse

# Scrapping
import requests
import justext
from bs4 import BeautifulSoup
from youtube_transcript_api import YouTubeTranscriptApi

# Tfidf et cos similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stopwords
from spacy.lang.en.stop_words import STOP_WORDS as en_stopwords
from sklearn.metrics.pairwise import cosine_similarity

def extract_youtube(api_key: str, link: str) -> tuple:
    """
    Scrap metadata from youtube video : date, title and author


    Parameters
    ----------
    api_key : str
        Api key of Youtube API
    link : str
        Url of the Youtube video

    Returns
    -------
    tuple
        Tuple containing date, title, author and transcript of the Youtube video.

    """
    origin_link = requests.get(link)
    youtube = build('youtube', 'v3',developerKey=api_key)
    matching_1 = re.match(r"(.+v=)(.+)", origin_link.url)
    lien, id_video = matching_1.groups()
    l_transcript = YouTubeTranscriptApi.get_transcript(id_video, languages=('en', 'fr', 'es'))
    transcript = ""
    for text in l_transcript:
        transcript += text["text"]
    request = youtube.videos().list(part="snippet", id=id_video)
    response = request.execute()
    print(response)
    date = response["items"][0]["snippet"]["publishedAt"]
    date = date.split("T")
    titre = response["items"][0]["snippet"]["title"]
    author = response["items"][0]["snippet"]["channelTitle"]
    return date[0], titre, author, transcript
    
def chatbot(luis_key: str, query: str) -> tuple:
    """
    Detect intent and entities from user input


    Parameters
    ----------
    luis_key : str
        Api key of Azure LUIS service
    query : str
        User input

    Returns
    -------
    tuple
        Tuple containing intent, format, theme and keywords in the user input.

    """
    params = urllib.parse.urlencode({"": query})
    url = f"https://luisaragrenoble.cognitiveservices.azure.com/luis/prediction/v3.0/apps/296432c0-75a8-452e-b9f2-900416c422ab/slots/staging/predict?verbose=true&show-all-intents=true&log=true&subscription-key={luis_key}&query={params}"
    
    # Get HTML content from URL
    r = requests.get(url)
    
    # returns a promise which resolves with the result of parsing the body text as JSON
    data = r.json()
    
    intent = data["prediction"]["topIntent"]
    ########## changements #############
    if 'theme' in data["prediction"]["entities"].keys():
        theme = data["prediction"]["entities"]["theme"][0][0]
    else:
        theme = 'No theme detected'
    ##################### BRICOLAGE #####################
    if "keyword" in data["prediction"]["entities"].keys():
        keywords = data["prediction"]["entities"]["keyword"]
    else:
        keywords = []
    if "format" in data["prediction"]["entities"].keys():
        format = data["prediction"]["entities"]["format"][0][0]
        
        # it may have multiple formats and themes, will do the same treament for themes later
        return intent, [format,], theme, keywords
    else:
        return intent, ["Video", "Texte", "Audio"] , theme, keywords

def get_metadata(url: str) -> dict:
    """
    Scrap title and description of web page


    Parameters
    ----------
    url : str
        Url of the web page to scrap

    Returns
    -------
    dict
        Dict containing the title, the description and the content of the web page. If not found, it's specified by "Not found" value.

    """
    # Get HTML content from URL
    response = requests.get(url)

    # Load HTML content with BeautifulSoup
    soup = BeautifulSoup(response.text, "html.parser")

    # Select title and description tags
    title = soup.select_one("head > title")
    description = soup.select_one("meta[name='description']")
    
    if title is None:
        title = "No title found."
    else:
        title = title.text

    if description is None:
        description = soup.select_one("meta[property='og:description']")
        if description is None:
            description = "No description found."
        else:
            description = description['content']
    else:
        description = description['content']
    
    return {"title": title,
            "description": description}
    
    
def get_content(url: str) -> dict:
    """
    Scrap text content of web page
    
    
    Parameters
    ----------
    url : str
        Url of the web page to scrap

    Returns
    -------
    dict
        Dict containing the content of the web page.

    """
    response = requests.get(url)
    paragraphs = justext.justext(response.content, justext.get_stoplist("English"))
    content = ""
    for paragraph in paragraphs:
        if not paragraph.is_boilerplate:
            content += paragraph.text.lower()
    return {"content": content}

def get_domain_url(url: str) -> str:
    """
    Get domain name of an url

    Parameters
    ----------
    url : str
        Url of the web page to scrap

    Returns
    -------
    str
        String containing the domain name of the url.

    """
    # on récupère l'url d'origine au cas où elle est raccourcie
    origin_url = requests.get(url)
    res = re.search(r'https?://([A-Za-z_0-9.-]+).*', origin_url.url)
    return res.group(1)


def get_cos_similarity(corpus: list, keywords: str):
    """
    Get cosine similarity between the corpus and keywords

    Parameters
    ----------
    corpus : list
        List of text content of resources to process
    keywords : str
        Keywords that we want to know the similarity with all resources content

    Returns
    -------
    numpy.ndarray
        Array with cosine similarities between keywords and resources content

    """
    stopwords_en = list(en_stopwords)
    stopwords_fr = list(fr_stopwords)

    data = corpus
    data.append(keywords.lower())
    vectorizer = TfidfVectorizer(stop_words=stopwords_fr + stopwords_en, use_idf=True)
    X = vectorizer.fit_transform(data)
    sim = cosine_similarity(X)
    return sim[-1,:-1]


def add_theme_luis(theme: str, auth_key: str, auth_endpoint="luisaragrenoble-authoring.cognitiveservices.azure.com/", appID="296432c0-75a8-452e-b9f2-900416c422ab", version="0.1", clEntityID="7aded760-1b3a-4f96-8a1b-fca3ab8f87ec"):
	"""
	Adds new theme to Luis


	Parameters
	----------
	theme : str
		New theme added by the user
	auth_key : str
		Authoring resource key
	auth_endpoint : str
		Authoring resource endpoint
	appID : str
		App Id
	version : str
		App version
	clEntityID : str
		Id of the closed list entity

	Returns
	-------
	int
		Response status code

	"""

	url = f"https://{auth_endpoint}luis/authoring/v3.0-preview/apps/{appID}/versions/{version}/closedlists/{clEntityID}/subLists"

	headers = {
		'Ocp-Apim-Subscription-Key': auth_key,
	}
	body = {
		"canonicalForm": theme,
		"list": [
		]
	}

	response = requests.post(url=url, json=body, headers=headers)

	return response.status_code


def train_luis(auth_key, auth_endpoint="luisaragrenoble-authoring.cognitiveservices.azure.com/", appID="296432c0-75a8-452e-b9f2-900416c422ab", version="0.1"):
    url = f"https://{auth_endpoint}/luis/authoring/v3.0-preview/apps/{appID}/versions/{version}/train"
    headers = {
		'Ocp-Apim-Subscription-Key': auth_key,
	  }
  
    requests.post(url=url, headers=headers)
    
    training_status = requests.get(url=url, headers=headers)
    
    return training_status.status_code


def publish_luis(auth_key, auth_endpoint="luisaragrenoble-authoring.cognitiveservices.azure.com/", appID="296432c0-75a8-452e-b9f2-900416c422ab"):
    url = f"https://{auth_endpoint}/luis/authoring/v3.0-preview/apps/{appID}/publish"
    headers = {
        'Content-Type': 'application/json',
		'Ocp-Apim-Subscription-Key': auth_key,
	  }
    body = {
        "isStaging": True,
        'versionId': "0.1",
    }
  
    response = requests.post(url=url,json=body, headers=headers)
    return response.status_code



	
    
