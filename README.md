# Projet 5 - Groupe 1

> Marouan, Thienvu, Gabriel

# Description de l'Application

Application Django implémentant un système de veille technologique intelligent.

L'objectif est d'évaluer la possibilité de mettre en place un outil de veille technologique permettant de faciliter la recherche de ressources sauvegardées par l'utilisateur via une intéraction avec un agent conversationnel (chatbot).

L'utilisateur peut :
* Créer un compte utilisateur / Se connecter / Se déconnecter
* Enregistrer une nouvelle ressource associée à un thème, un format (texte vs vidéo vs audio) et des commentaires
* Ajouter un nouveau thème
* Intéragir avec un chatbot pour demander des ressources spécifiques

Le chatbot peut :
* Repérer l'intention de l'utilisateur dans sa requête (e.g. chercher une ressource)
* Extraire des entités (format, thème, mots-clés) dans la requête utilisateur
* Afficher les ressources demandées pertinentes vis-à-vis des entités extraites


# Lancer l'application en local

Cloner le projet gitlab
```bash
git clone url
```

Installer l'environnement virtuel avec ses dépendances
```bash
pipenv install
```

Instancier le conteneur docker permettant l'accès à la base de données Postgres
```bash
sudo docker-compose up -d
```

Effectuer les migrations Django pour générer la structure de la base de données
```bash
python manage.py makemirations
python manage.py migrate
```

Démarrer l'application
```bash
python manage.py runserver
```





















documentation:
https://docs.djangoproject.com/fr/3.1/intro/tutorial02/
https://serpapi.com/?gclid=CjwKCAjw9qiTBhBbEiwAp-GE0Y6cAyRIHKaP_ErkCgmLWT5ECyuGjpFpYGxM3DtzOj3yXACYq-ysuxoCVn8QAvD_BwE
https://learndjango.com/tutorials/django-login-and-logout-tutorial

# gérer l'enregistrement d'un nouvel utilisateur
https://ordinarycoders.com/blog/article/django-user-register-login-logout

# gérer le système de messages

https://www.csestack.org/display-messages-form-submit-django/
https://stackoverflow.com/questions/42893623/django-success-message-in-view-using-django-contrib-messages
https://docs.djangoproject.com/fr/4.0/ref/contrib/messages/

# créer une base postGres
https://gitlab.com/simplon-dev-data/grenoble-2020-2021/projets/projet_4/projet_4-dbt/-/blob/activite/DecouverteDBT.md

# récupérer une information d'un formulaire sans passer par un modèle
https://docs.djangoproject.com/en/dev/topics/forms/#form-objects
https://stackoverflow.com/questions/17754295/can-i-have-a-django-form-without-model
