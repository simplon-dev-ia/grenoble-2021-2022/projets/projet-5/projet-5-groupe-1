"""AIstorical URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
#from django.views.generic.base import TemplateView

urlpatterns = [
    
    # on garde l'accès à l'application historical qui implique un login
    path('historical/', include('historical.urls')),
    
    # on garde l'accès à la partie admin pour se déconnecter facilement
    path('admin/', admin.site.urls),
    
    # on garde l'accès aux fonctionnalités de compte pour pouvoir se logger
    path('accounts/', include('django.contrib.auth.urls')),
    
    
]
